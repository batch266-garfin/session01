import java.util.Scanner;
public class Activity {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String firstName, lastName;
        Double firstSubj, secondSubj, thirdSubj;

        System.out.println("First Name:");
        firstName = scanner.nextLine();
        System.out.println("Last Name:");
        lastName = scanner.nextLine();
        System.out.println("First Subject Grade:");
        firstSubj = scanner.nextDouble();
        System.out.println("Second Subject Grade:");
        secondSubj = scanner.nextDouble();
        System.out.println("Third Subject Grade:");
        thirdSubj = scanner.nextDouble();

        Double avg = (firstSubj+secondSubj+thirdSubj)/3;

        System.out.println("Good Day, "+ firstName + " " + lastName);
        System.out.println("Your grade is: " + avg);

    }
}
