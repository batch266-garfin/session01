package com.zuitt.example;

public class Variables {

    public static void main(String[] args) {

        /*
        * Data Types:
        * Primitive
        * - int for Integer
        * - double for float
        * - char for single characters
        * - boolean for boolean values
        *
        * Non-Primitive
        * - String
        * - Arrays
        * - Classes
        * - Interface
        * */

        int age = 18;
        char middle_initial = 'V';
        boolean isLegalAge = true;

        System.out.println("The age is "+ age);
        System.out.println("The middle name is "+ middle_initial);
        System.out.println("The user is of legal age? "+ isLegalAge);
    }
}
