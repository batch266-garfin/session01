package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {

    public static void main(String[] args) {

        //System.out.println("2" + 2);
        Scanner userInput = new Scanner(System.in);

        /*System.out.println("How old are you?");
        String userAge = age.nextLine();
        System.out.println("The age is "+ userAge);*/

        System.out.println("How old are you?");
        double age = new Double(userInput.nextLine());

        //int ages = userInput.nextInt();

        System.out.println("This is your age: "+ age);

    }
}
